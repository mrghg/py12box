**AGAGE 12-box model**

This is a Python version of the AGAGE 12-box model described (first as a 9-box model) in Cunnold et al., 1983. Subsequently modified in Cunnold et al., 1994 and Rigby et al., 2013. This version is based on the IDL version from Rigby et al., 2013. 

This repo is no longer maintained. Please go to Github [https://github.com/mrghg/py12box](https://github.com/mrghg/py12box)
